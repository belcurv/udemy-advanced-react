Going to generate a new React project using Create React App. Boo.

Testing flow:
1. Look at each individual part of your application
2. Imagine telling a friend "here's what thispiece of code does"
3. Write a test to verify each part does what you expect.

## Jest

Docs: https://jestjs.io/docs/en/expect.html

## Enzyme

Docs: airbnb.io/enzyme

### Enzyme API

Renderers:
1. Static: pass a component to it and render the given component returning plain HTML. Can't simulate clicks, etc.
2. Shallow: render **just** the given component and none of its children. All DOM in the given component is rendered, just none of the DOM in its children. Used for testing one component by itself in isolation.
3. Full DOM: render the component and all of its children, plus let us modify it afterwards. Can simulate click events, input text, etc.

### Fake Comments API

https://jsonplaceholder.typicode.com

Need some additional packages for making and testing HTTP requests:

```
npm i axios redux-promise moxios
```

