/* global it expect */

import commentsReducer from './comments'
import { SAVE_COMMENT } from '../../actions/types'

it('should handle actions of type SAVE_COMMENT', () => {
  const action = {
    type: SAVE_COMMENT,
    payload: 'New comment'
  }
  const newState = commentsReducer([], action)
  expect(newState).toEqual(['New comment'])
})

it('should return original state from unknown action type', () => {
  const newState = commentsReducer([], { type: 'UNKNOWN' })
  expect(newState).toEqual([])
})
