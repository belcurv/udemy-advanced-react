import { SAVE_COMMENT, FETCH_COMMENTS } from '../../actions/types'
const initialState = []

export default function (state = initialState, action) {
  switch (action.type) {
    case SAVE_COMMENT:
      return [...state, action.payload]

    case FETCH_COMMENTS:
      // const comments = action.payload.data.map(comment => comment.name)
      // just return the names of the 1st 10 comments
      const comments = action.payload.data.reduce((result, comment, i) => {
        if (i < 10) {
          result.push(comment.name)
        }
        return result
      }, [])
      return [...state, ...comments]

    default:
      return state
  }
}
