/**
 * Jest will look for this file and execute it before any other tests
 * are run.
 *
 * We use it to configure Enzyme.
*/

import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })
