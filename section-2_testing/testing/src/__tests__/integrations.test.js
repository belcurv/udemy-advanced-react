/* global beforeEach afterEach it expect */

/**
 * We need to mock axios because we don't have a real browser.
 * We trick axios into thinking it instantly rec'd a real response.
 */

import React from 'react'
import { mount } from 'enzyme'
import moxios from 'moxios'
import Root from '../Root'
import App from '../components/App'

beforeEach(() => {
  moxios.install() // intercept axios requests
  moxios.stubRequest('http://jsonplaceholder.typicode.com/comments', {
    status: 200,
    response: [
      { name: 'id labore ex et quam laborum' },
      { name: 'quo vero reiciendis velit similique earum' },
      { name: 'odio adipisci rerum aut animi' }
    ]
  })
})

afterEach(() => {
  moxios.uninstall()
})

it('can getch a list of comments and display them', (done) => {
  const wrapped = mount(
    <Root>
      <App />
    </Root>
  )

  wrapped.find('.fetch-comments').simulate('click')

  // pause between API call and expectation for moxios to do its thing
  moxios.wait(() => {
    wrapped.update() // rerender component
    expect(wrapped.find('li')).toHaveLength(3)
    done() // needed because test is async
    wrapped.unmount() // destroy the component
  })
})
