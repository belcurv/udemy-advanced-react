/* global beforeEach afterEach it expect */

import React from 'react'
import { mount } from 'enzyme'
import Root from '../../Root'
import CommentList from './CommentList'

const comments = ['Comment 1', 'Comment 2']
let wrapped

beforeEach(() => {
  const initialState = { comments }
  wrapped = mount(
    <Root initialState={initialState}>
      <CommentList />
    </Root>
  )
})

afterEach(() => {
  wrapped.unmount()
})

it('should show one <li> per comment', () => {
  expect(wrapped.find('li')).toHaveLength(comments.length)
})

it('should show each comment\'s text', () => {
  expect(wrapped.render().text()).toContain(comments[0])
  expect(wrapped.render().text()).toContain(comments[1])
})
