import React from 'react'
import './App.css'
import CommentBox from './CommentBox'
import CommentList from './CommentList'

const App = () => (
  <div>
    <CommentBox />
    <CommentList />
  </div>
)

export default App
