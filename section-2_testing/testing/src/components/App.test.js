/* global beforeEach it expect */

import React from 'react'
import { shallow } from 'enzyme'

import App from './App'
import CommentBox from './CommentBox'
import CommentList from './CommentList'

let wrapped

beforeEach(() => {
  wrapped = shallow(<App />)
})

it('should contain one CommentBox component', () => {
  expect(wrapped.find(CommentBox)).toHaveLength(1)
})

it('should contain one CommentList component', () => {
  expect(wrapped.find(CommentList)).toHaveLength(1)
})
