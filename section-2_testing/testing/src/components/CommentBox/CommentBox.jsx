import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { saveComment, fetchComments } from '../../actions'

export class CommentBox extends React.Component {
  constructor (props) {
    super(props)
    this.state = { comment: '' }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange (event) {
    this.setState({ comment: event.target.value })
  }

  handleSubmit (event) {
    event.preventDefault()
    this.props.saveComment(this.state.comment)
    this.setState({ comment: '' })
  }

  render () {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <h4>Add a Comment</h4>
          <textarea
            value={this.state.comment}
            onChange={this.handleChange}
          />
          <div>
            <button>Submit Comment</button>
          </div>
        </form>
        <button className='fetch-comments' onClick={this.props.fetchComments}>
          Fetch Comments
        </button>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  saveComment,
  fetchComments
}, dispatch)

export default connect(null, mapDispatchToProps)(CommentBox)
