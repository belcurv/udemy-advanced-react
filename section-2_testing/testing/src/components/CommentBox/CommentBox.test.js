/* global beforeEach afterEach describe it expect */

/**
 * Example using full DOM rendering. The `mount`ed component is not auto
 * cleaned up like shallow renderer, so we have to explicitly call
 * `unmount` on the component after each test.
 */

import React from 'react'
import { mount } from 'enzyme'
import Root from '../../Root'
import CommentBox from './CommentBox'

let wrapped

beforeEach(() => {
  wrapped = mount(
    <Root>
      <CommentBox />
    </Root>
  )
})

afterEach(() => {
  wrapped.unmount()
})

it('should have a text area and 2 buttons', () => {
  expect(wrapped.find('textarea')).toHaveLength(1)
  expect(wrapped.find('button')).toHaveLength(2)
})

describe('textarea interaction events', () => {
  beforeEach(() => {
    const eventMock = {
      target: { value: 'new test comment' }
    }
    wrapped
      .find('textarea')
      .simulate('change', eventMock)
      .update()
  })

  it('allows users to input text into the textarea, and displays their text', () => {
    // assert that the 'value' prop on textarea equals the mock value
    expect(wrapped.find('textarea').prop('value')).toEqual('new test comment')
  })

  it('should empty the textarea on form submit', () => {
    // assert that textarea value is not null
    expect(wrapped.find('textarea').prop('value')).toEqual('new test comment')

    wrapped
      .find('form') // find the form element
      .simulate('submit') // simulate a 'submit' event
      .update() // force the component to update

    // assert that textarea value has been emptied
    expect(wrapped.find('textarea').prop('value')).toEqual('')
  })
})
