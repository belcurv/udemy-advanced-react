# Authentication

To begin, we're building a JSON API.

### Cookies vs Tokens

HTTP protocol is stateless. Cookies are a way to add 'state' to HTTP. Servers can place info on user's cookie (user ID, etc). Followup requests by the same user would include the same cookie.

Cookies:

* Automatically included in each request
* Unique to each domain. Cookies for `google.com` cannot be shared with `facebook.com`.
* Cannot be sent to different domains

```
Request
  Headers:
    cookie: {}
  Body:
    { color: 'red' }
```

Tokens:

* Have to manually wire up in all requests
* Can be sent to any domain

```
Request
  Headers:
    authorization: 'jas7f7e759sjdl...'
  Body:
    { color: 'red' }
```

