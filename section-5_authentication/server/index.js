'use strict'

require('dotenv').config()
const ENV = process.env.NODE_ENV || 'development'
const PORT = process.env.PORT || 3090

const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const mongoose = require('mongoose')

const dbConfig = require('./db')[ENV]

const router = require('./router')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json({ type: '*/*' }))

// routes
app.use('/', router)

mongoose.connect(dbConfig.url, { useNewUrlParser: true })
  .then(() => {
    console.log(`Connected to ${dbConfig.url}`)
    app.listen(PORT, () => console.log('Server listening on port:', PORT))
  })
  .catch((err) => {
    console.log(err.message)
    process.exit(1)
  })
