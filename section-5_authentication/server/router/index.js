const router = require('express').Router()
const { signup } = require('../controllers/auth_ctrl')

router.post('/signup', signup)

exports = module.exports = router
