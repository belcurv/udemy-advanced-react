const User = require('../models/user')
const jwt = require('jsonwebtoken')
const jwtSecret = process.env.JWT_SECRET

/* ================================ HELPERS ================================ */

const generateJWT = (payload) => {
  const token = jwt.sign(
    { payload }, // payload
    jwtSecret,
    { expiresIn: '7d' } // options
  )
  return token
}

/* ============================ ROUTE HANDLERS ============================= */

const login = async (req, res, next) => {
  const { email, password } = req.body

  if (!email || !password) {
    return res.status(400).json({ error: 'You must provide email & password' })
  }

  const user = await User.findOne({ email })
    .catch(err => next(err))

  if (!user) {
    return res.status(400).json({ error: 'User not found' })
  }
}

const signup = async (req, res, next) => {
  const { email, password } = req.body

  if (!email || !password) {
    return res.status(400).json({ error: 'You must provide email & password' })
  }

  const user = await User.findOne({ email })
    .catch(err => next(err))

  if (user) {
    return res.status(400).json({ error: 'Email already in use' })
  }

  const newUser = new User({ email, password })

  newUser.save()
    .then(async () => {
      const payload = {
        sub: newUser._id, // subject
        iat: new Date().getTime() // issued-at time
      }
      const token = await generateJWT(payload)
      return res.status(200).json({ success: true, token })
    })
    .catch(err => next(err))
}

/* ================================ EXPORTS ================================ */

exports = module.exports = {
  signup,
  login
}
