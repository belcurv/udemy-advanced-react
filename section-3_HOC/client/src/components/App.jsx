import React from 'react'
import { Route, Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import CommentBox from './CommentBox'
import CommentList from './CommentList'

import { changeAuth } from '../actions'

class App extends React.Component {
  renderButton () {
    return this.props.auth.isLoggedIn
      ? (<button onClick={() => this.props.changeAuth(false)}>Sign Out</button>)
      : (<button onClick={() => this.props.changeAuth(true)}>Sign In</button>)
  }

  renderHeader () {
    return (
      <ul>
        <li>
          <Link to='/'>Home</Link>
        </li>
        <li>
          <Link to='/post'>Post A Comment</Link>
        </li>
        <li>{this.renderButton()}</li>
      </ul>
    )
  }

  render () {
    return (
      <div>
        {this.renderHeader()}
        <Route path='/post' component={CommentBox} />
        <Route exact path='/' component={CommentList} />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  changeAuth
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(App)
