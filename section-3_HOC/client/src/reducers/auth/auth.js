/**
 * Super simple fake auth - we just store a boolean to represent whether
 * a user is logged in or not
*/

import { CHANGE_AUTH } from '../../actions/types'

const defaultState = {
  isLoggedIn: false
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case CHANGE_AUTH:
      return {
        ...state,
        isLoggedIn: action.payload
      }
    default:
      return state
  }
}
