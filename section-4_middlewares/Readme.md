# Middleware

The React / Redux execution cycle:

1. React
   ... calls an ...
2. Action Creator
   .... returns a ...
3. Action object
   ... sent to ...
4. Middleware
   ... does something and forwards action to ...
5. Reducers
   ... produces new ...
6. State
   ... is sent to ...
1. React
   ... repeat ...

So, the purpose is to inspect the redux action, and do other things with it before passing the action to the reducer.

No upper limit to the number of middlewares we can use. Multiple middleware is referred to as the middleware stack.

The middleware stack is a chain of middlewares that execute (inspect an action) in a kind of loop.

Each middleware in the stack looks at the incoming action and decides whether to pass it on, or to take action. If it takes action, it eventually returns / resolves with a NEW action object of the same type, but with a new payload. This goes back in at the top of the middleware stack, going through all our middlewares again.

Middlewares return new actions so that their order doesn't matter. We don't have to order them in any way -- sending the new action through all middlewares means we don't have to arbitrarily rearrange middleware to make sure they all operate correctly.

### JSON Schema

A standard for validating JSON documents.

JSON schema generation tool: https://jsonschema.net/

We create a schema definition file. Then our JSON schema validation library (`tv4`) uses it to validate actual redux state / app data.

### tv4 (Tiny Validator 4)

Takes some data, our JSON schema, and validates it.