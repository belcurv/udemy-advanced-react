export default {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  $id: 'http://example.com/root.json',
  type: 'object',
  title: 'The Root Schema',
  required: [
    'auth',
    'comments'
  ],
  properties: {
    auth: {
      $id: '#/properties/auth',
      type: 'object',
      title: 'The Auth Schema',
      required: [
        'isLoggedIn'
      ],
      properties: {
        isLoggedIn: {
          $id: '#/properties/auth/properties/isLoggedIn',
          type: 'boolean',
          title: 'The Isloggedin Schema',
          default: false,
          examples: [
            true
          ]
        }
      }
    },
    comments: {
      $id: '#/properties/comments',
      type: 'array',
      title: 'The Comments Schema',
      items: {
        $id: '#/properties/comments/items',
        type: 'string',
        title: 'The Items Schema',
        default: '',
        examples: [
          'Comment 1',
          'Comment 2'
        ],
        pattern: '^(.*)$'
      }
    }
  }
}
