# Middleware

Middleware to allow us to return promises in the payload property of our action creators.

Redux `dispatch`. Actions are passed to dispatch, who forwards it into the middleware stack.

### Redux middleware boilerplate signature:

```javascript
export default function ({ dispatch }) {
  return function (next) {
    return function (action) {
      // evaluation logic
    }
  }
}
```

Aka ...

```javascript
export default ({ dispatch }) => (next) => (action) => {
  // evaluation logic
}
```
