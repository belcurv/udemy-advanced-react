export default ({ dispatch }) => (next) => (action) => {
  // Check to see if action has a promise on payload property
  if (!action.payload || !action.payload.then) {
    // if it does not, forward action to the next middleware
    return next(action)
  }
  // If it does, wait for it to resolve with data
  action.payload
    .then((response) => {
      // create a new action with the original action's properties
      // and replace the payload with our response
      const newAction = { ...action, payload: response }
      dispatch(newAction)
    })
}
